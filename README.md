# Quentin Quarantino

## A training under the constraints of the Covid epidemic

A Repo to allow us, the trainers, to follow your progression and check your PRs.

- [Quentin Quarantino](#quentin-quarantino)
  - [A training under the constraints of the Covid epidemic](#a-training-under-the-constraints-of-the-covid-epidemic)
  - [Preparation](#preparation)
  - [How To Use](#how-to-use)

## Preparation

You should have a VM linux or use wsl which is more recommended.
Create you own fork this repo.

- [WSL installation](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
- [How to create a fork in GitLab](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)

## How To Use

It's quiet simple. In the `docs` folder you have the exercise (order in
[index.md](docs/index.md)).
Once you understand the exercise you can start it! write your code in the
matching file in the `src` folder.
After you have checked your code made sure it's linted correctly and tested
your code you can open a PR (You should notify us in some way that you have
finished) to *this* repo (not the one you have forked)
